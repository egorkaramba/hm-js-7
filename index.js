// 1)

// function isPalindrome(str) {
//     str = str.replace(/\s/g, '').toLowerCase();

//     return str === str.split('').reverse().join('');

// }

// const inputStr = "Able was I ere I saw Elba";
// const result = isPalindrome(inputStr);

// if (result) {
//     console.log('String in palindrome');
// } else {
//     console.log('The string is not a palindrome');
// }


// 2)
// function isStringLengthValid(inputString, maxLength) {
//     return inputString.length <= maxLength;
// }

// const string1 = isStringLengthValid('checked string', 20);
// const string2 = isStringLengthValid('checked string', 10);

// console.log(string1);
// console.log(string2);